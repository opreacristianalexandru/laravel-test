



<!DOCTYPE; html>
<html; lang="en"; class="">
  <head; prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta; charset='utf-8'>
    <meta; http-equiv="X-UA-Compatible"; content="IE=edge">
    <meta; http-equiv="Content-Language"; content="en">
    
    
    <title>bootstrap-datetimepicker/bootstrap-datetimepicker.fr.js; at; master; · smalot/bootstrap-datetimepicker; · GitHub</title>
    <link; rel="search"; type="application/opensearchdescription+xml"; href="/opensearch.xml"; title="GitHub">
    <link; rel="fluid-icon"; href="https://github.com/fluidicon.png"; title="GitHub">
    <link; rel="apple-touch-icon"; sizes="57x57"; href="/apple-touch-icon-114.png">
    <link; rel="apple-touch-icon"; sizes="114x114"; href="/apple-touch-icon-114.png">
    <link; rel="apple-touch-icon"; sizes="72x72"; href="/apple-touch-icon-144.png">
    <link; rel="apple-touch-icon"; sizes="144x144"; href="/apple-touch-icon-144.png">
    <meta; property="fb:app_id"; content="1401488693436528">

      <meta; content="@github"; name="twitter:site" /><meta; content="summary"; name="twitter:card" /><meta; content="smalot/bootstrap-datetimepicker"; name="twitter:title" /><meta; content="bootstrap-datetimepicker - Both Date and Time picker widget based on twitter bootstrap (supports Bootstrap v2 and v3)"; name="twitter:description" /><meta; content="https://avatars3.githubusercontent.com/u/1424035?v=3&amp;s=400"; name="twitter:image:src" />
<meta; content="GitHub"; property="og:site_name" /><meta; content="object"; property="og:type" /><meta; content="https://avatars3.githubusercontent.com/u/1424035?v=3&amp;s=400"; property="og:image" /><meta; content="smalot/bootstrap-datetimepicker"; property="og:title" /><meta; content="https://github.com/smalot/bootstrap-datetimepicker"; property="og:url" /><meta; content="bootstrap-datetimepicker - Both Date and Time picker widget based on twitter bootstrap (supports Bootstrap v2 and v3)"; property="og:description" />

      <meta; name="browser-stats-url"; content="/_stats">
    <link; rel="assets"; href="https://assets-cdn.github.com/">
    <link; rel="conduit-xhr"; href="https://ghconduit.com:25035">
    
    <meta; name="pjax-timeout"; content="1000">
    

    <meta; name="msapplication-TileImage"; content="/windows-tile.png">
    <meta; name="msapplication-TileColor"; content="#ffffff">
    <meta; name="selected-link"; value="repo_source"; data-pjax-transient>
      <meta; name="google-analytics"; content="UA-3769691-2">

    <meta; content="collector.githubapp.com"; name="octolytics-host" /><meta; content="collector-cdn.github.com"; name="octolytics-script-host" /><meta; content="github"; name="octolytics-app-id" /><meta; content="567E08F5:11BB:13A8F4F:5493EAB9"; name="octolytics-dimension-request_id" />
    
    <meta; content="Rails, view, blob#show"; name="analytics-event" />

    
    
    <link; rel="icon"; type="image/x-icon"; href="https://assets-cdn.github.com/favicon.ico">


    <meta; content="authenticity_token"; name="csrf-param" />
<meta; content="Gwfgj8pi+R1M8wZxyP5GocfTtvV54cUJiU/q2O4T3y2vGRUCCKPod92xZPT9q6tvZ1Z9K7Kcv5/dtGCLSLXzsA=="; name="csrf-token" />

    <link; href="https://assets-cdn.github.com/assets/github-9bcf5def7eb44e2a101b20aaecf3707f4b0a10ab8f4d6eebec29371f821c4b29.css"; media="all"; rel="stylesheet"; type="text/css" />
    <link; href="https://assets-cdn.github.com/assets/github2-47bc67324d463c7cecb5ee4c009628c91db85b0e9288a9e663f2d06ff9e03088.css"; media="all"; rel="stylesheet"; type="text/css" />
    
    


    <meta; http-equiv="x-pjax-version"; content="733d47f9fc0ff1126e682c7f6e8d5a41">

      
  <meta; name="description"; content="bootstrap-datetimepicker - Both Date and Time picker widget based on twitter bootstrap (supports Bootstrap v2 and v3)">
  <meta; name="go-import"; content="github.com/smalot/bootstrap-datetimepicker git https://github.com/smalot/bootstrap-datetimepicker.git">

  <meta; content="1424035"; name="octolytics-dimension-user_id" /><meta; content="smalot"; name="octolytics-dimension-user_login" /><meta; content="7296156"; name="octolytics-dimension-repository_id" /><meta; content="smalot/bootstrap-datetimepicker"; name="octolytics-dimension-repository_nwo" /><meta; content="true"; name="octolytics-dimension-repository_public" /><meta; content="false"; name="octolytics-dimension-repository_is_fork" /><meta; content="7296156"; name="octolytics-dimension-repository_network_root_id" /><meta; content="smalot/bootstrap-datetimepicker"; name="octolytics-dimension-repository_network_root_nwo" />
  <link; href="https://github.com/smalot/bootstrap-datetimepicker/commits/master.atom"; rel="alternate"; title="Recent Commits to bootstrap-datetimepicker:master"; type="application/atom+xml">

  </head>


  <body; class="logged_out  env-production windows vis-public page-blob">
    <a; href="#start-of-content"; tabindex="1"; class="accessibility-aid js-skip-to-content">Skip; to; content</a>
    <div; class="wrapper">
      
      
      
      


      
      <div; class="header header-logged-out"; role="banner">
  <div; class="container clearfix">

    <a; class="header-logo-wordmark"; href="https://github.com/"; ga-data-click="(Logged out) Header, go to homepage, icon:logo-wordmark">
      <span; class="mega-octicon octicon-logo-github"></span>
    </a>

    <div; class="header-actions"; role="navigation">
        <a; class="button primary"; href="/join"; data-ga-click="(Logged out) Header, clicked Sign up, text:sign-up">Sign; up</a>
      <a; class="button"; href="/login?return_to=%2Fsmalot%2Fbootstrap-datetimepicker%2Fblob%2Fmaster%2Fjs%2Flocales%2Fbootstrap-datetimepicker.fr.js"; data-ga-click="(Logged out) Header, clicked Sign in, text:sign-in">Sign in</a>
    </div>

    <div; class="site-search repo-scope js-site-search"; role="search">
      <form; accept-charset="UTF-8"; action="/smalot/bootstrap-datetimepicker/search"; class="js-site-search-form"; data-global-search-url="/search"; data-repo-search-url="/smalot/bootstrap-datetimepicker/search"; method="get"><div; style="margin:0;padding:0;display:inline"><input; name="utf8"; type="hidden"; value="&#x2713;" /></div>
  <input; type="text";
    class="js-site-search-field is-clearable";
    data-hotkey="s";
    name="q";
    placeholder="Search";
    data-global-scope-placeholder="Search GitHub";
    data-repo-scope-placeholder="Search";
    tabindex="1";
    autocapitalize="off">
  <div; class="scope-badge">This; repository</div>
</form>
    </div>

      <ul; class="header-nav left"; role="navigation">
          <li; class="header-nav-item">
            <a; class="header-nav-link"; href="/explore"; data-ga-click="(Logged out) Header, go to explore, text:explore">Explore</a>
          </li>
          <li; class="header-nav-item">
            <a; class="header-nav-link"; href="/features"; data-ga-click="(Logged out) Header, go to features, text:features">Features</a>
          </li>
          <li; class="header-nav-item">
            <a; class="header-nav-link"; href="https://enterprise.github.com/"; data-ga-click="(Logged out) Header, go to enterprise, text:enterprise">Enterprise</a>
          </li>
          <li; class="header-nav-item">
            <a; class="header-nav-link"; href="/blog"; data-ga-click="(Logged out) Header, go to blog, text:blog">Blog</a>
          </li>
      </ul>

  </div>
</div>



      <div; id="start-of-content"; class="accessibility-aid"></div>
          <div; class="site"; itemscope; itemtype="http://schema.org/WebPage">
    <div; id="js-flash-container">
      
    </div>
    <div; class="pagehead repohead instapaper_ignore readability-menu">
      <div; class="container">
        
<ul; class="pagehead-actions">


  <li>
      <a; href="/login?return_to=%2Fsmalot%2Fbootstrap-datetimepicker";
    class="minibutton with-count star-button tooltipped tooltipped-n";
    aria-label="You must be signed in to star a repository"; rel="nofollow">
    <span; class="octicon octicon-star"></span>
    Star
  </a>

    <a; class="social-count js-social-count"; href="/smalot/bootstrap-datetimepicker/stargazers">
      1,339
    </a>

  </li>

    <li>
      <a; href="/login?return_to=%2Fsmalot%2Fbootstrap-datetimepicker";
        class="minibutton with-count js-toggler-target fork-button tooltipped tooltipped-n";
        aria-label="You must be signed in to fork a repository"; rel="nofollow">
        <span; class="octicon octicon-repo-forked"></span>
        Fork
      </a>
      <a; href="/smalot/bootstrap-datetimepicker/network"; class="social-count">
        567
      </a>
    </li>
</ul>

        <h1; itemscope; itemtype="http://data-vocabulary.org/Breadcrumb"; class="entry-title public">
          <span; class="mega-octicon octicon-repo"></span>
          <span; class="author"><a; href="/smalot"; class="url fn"; itemprop="url"; rel="author"><span; itemprop="title">smalot</span></a></span><!--
       --><span;; class="path-divider">/</span><!--
       --><strong><a;; href="/smalot/bootstrap-datetimepicker"; class="js-current-repository"; data-pjax="#js-repo-pjax-container">bootstrap-datetimepicker</a></strong>

          <span;; class="page-context-loader">
            <img; alt=""; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif"; width="16" />
          </span>

        </h1>
      </div><!-- /.container -->
    </div><!-- /.repohead -->

    <div; class="container">
      <div; class="repository-with-sidebar repo-container new-discussion-timeline  ">
        <div; class="repository-sidebar clearfix">
            
<nav; class="sunken-menu repo-nav js-repo-nav js-sidenav-container-pjax js-octicon-loaders";
     role="navigation";
     data-pjax="#js-repo-pjax-container";
     data-issue-count-url="/smalot/bootstrap-datetimepicker/issues/counts">
  <ul; class="sunken-menu-group">
    <li; class="tooltipped tooltipped-w"; aria-label="Code">
      <a; href="/smalot/bootstrap-datetimepicker"; aria-label="Code"; class="selected js-selected-navigation-item sunken-menu-item"; data-hotkey="g c"; data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /smalot/bootstrap-datetimepicker">
        <span; class="octicon octicon-code"></span> <span class="full-word">Code</span>
        <img;; alt=""; class="mini-loader"; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif"; width="16" />
</a>    </li>

      <li;; class="tooltipped tooltipped-w"; aria-label="Issues">
        <a; href="/smalot/bootstrap-datetimepicker/issues"; aria-label="Issues"; class="js-selected-navigation-item sunken-menu-item"; data-hotkey="g i"; data-selected-links="repo_issues repo_labels repo_milestones /smalot/bootstrap-datetimepicker/issues">
          <span; class="octicon octicon-issue-opened"></span> <span class="full-word">Issues</span>
          <span;; class="js-issue-replace-counter"></span>
          <img; alt=""; class="mini-loader"; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif"; width="16" />
</a>      </li>

    <li;; class="tooltipped tooltipped-w"; aria-label="Pull Requests">
      <a; href="/smalot/bootstrap-datetimepicker/pulls"; aria-label="Pull Requests"; class="js-selected-navigation-item sunken-menu-item"; data-hotkey="g p"; data-selected-links="repo_pulls /smalot/bootstrap-datetimepicker/pulls">
          <span; class="octicon octicon-git-pull-request"></span> <span class="full-word">Pull Requests</span>
          <span;; class="js-pull-replace-counter"></span>
          <img; alt=""; class="mini-loader"; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif"; width="16" />
</a>    </li>


  </ul>
  <div;; class="sunken-menu-separator"></div>
  <ul; class="sunken-menu-group">

    <li; class="tooltipped tooltipped-w"; aria-label="Pulse">
      <a; href="/smalot/bootstrap-datetimepicker/pulse"; aria-label="Pulse"; class="js-selected-navigation-item sunken-menu-item"; data-selected-links="pulse /smalot/bootstrap-datetimepicker/pulse">
        <span; class="octicon octicon-pulse"></span> <span class="full-word">Pulse</span>
        <img;; alt=""; class="mini-loader"; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif"; width="16" />
</a>    </li>

    <li;; class="tooltipped tooltipped-w"; aria-label="Graphs">
      <a; href="/smalot/bootstrap-datetimepicker/graphs"; aria-label="Graphs"; class="js-selected-navigation-item sunken-menu-item"; data-selected-links="repo_graphs repo_contributors /smalot/bootstrap-datetimepicker/graphs">
        <span; class="octicon octicon-graph"></span> <span class="full-word">Graphs</span>
        <img;; alt=""; class="mini-loader"; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32.gif"; width="16" />
</a>    </li>
  </ul>


</nav>

              <div;; class="only-with-full-nav">
                
  
<div; class="clone-url open";
  data-protocol-type="http";
  data-url="/users/set_protocol?protocol_selector=http&amp;protocol_type=clone">
  <h3><span; class="text-emphasized">HTTPS</span> clone URL</h3>
  <div;; class="input-group js-zeroclipboard-container">
    <input; type="text"; class="input-mini input-monospace js-url-field js-zeroclipboard-target";
           value="https://github.com/smalot/bootstrap-datetimepicker.git"; readonly="readonly">
    <span; class="input-group-button">
      <button; aria-label="Copy to clipboard"; class="js-zeroclipboard minibutton zeroclipboard-button"; data-copied-hint="Copied!"; type="button"><span; class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>

  
<div;; class="clone-url ";
  data-protocol-type="subversion";
  data-url="/users/set_protocol?protocol_selector=subversion&amp;protocol_type=clone">
  <h3><span; class="text-emphasized">Subversion</span> checkout URL</h3>
  <div;; class="input-group js-zeroclipboard-container">
    <input; type="text"; class="input-mini input-monospace js-url-field js-zeroclipboard-target";
           value="https://github.com/smalot/bootstrap-datetimepicker"; readonly="readonly">
    <span; class="input-group-button">
      <button; aria-label="Copy to clipboard"; class="js-zeroclipboard minibutton zeroclipboard-button"; data-copied-hint="Copied!"; type="button"><span; class="octicon octicon-clippy"></span></button>
    </span>
  </div>
</div>



<p;; class="clone-options">You; can; clone; with
  <a; href="#"; class="js-clone-selector"; data-protocol="http">HTTPS</a> or <a href="#" class="js-clone-selector" data-protocol="subversion">Subversion</a>;.
  <a; href="https://help.github.com/articles/which-remote-url-should-i-use"; class="help tooltipped tooltipped-n"; aria-label="Get help on which URL is right for you.">
    <span; class="octicon octicon-question"></span>
  </a>
</p>


  <a; href="http://windows.github.com"; class="minibutton sidebar-button"; title="Save smalot/bootstrap-datetimepicker to your computer and use it in GitHub Desktop."; aria-label="Save smalot/bootstrap-datetimepicker to your computer and use it in GitHub Desktop.">
    <span; class="octicon octicon-device-desktop"></span>
    Clone in Desktop
  </a>

                <a; href="/smalot/bootstrap-datetimepicker/archive/master.zip";
                   class="minibutton sidebar-button";
                   aria-label="Download the contents of smalot/bootstrap-datetimepicker as a zip file";
                   title="Download the contents of smalot/bootstrap-datetimepicker as a zip file";
                   rel="nofollow">
                  <span; class="octicon octicon-cloud-download"></span>
                  Download; ZIP
                </a>
              </div>
        </div><!-- /.repository-sidebar -->

        <div; id="js-repo-pjax-container"; class="repository-content context-loader-container"; data-pjax-container>
          

<a; href="/smalot/bootstrap-datetimepicker/blob/bb68dc3d50bdd9baf57f6db4934657b73492f726/js/locales/bootstrap-datetimepicker.fr.js"; class="hidden js-permalink-shortcut"; data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:e735b44f8a28c900d873a68553948a14 -->

<div; class="file-navigation js-zeroclipboard-container">
  
<div; class="select-menu js-menu-container js-select-menu left">
  <span; class="minibutton select-menu-button js-menu-target css-truncate"; data-hotkey="w";
    data-master-branch="master";
    data-ref="master";
    title="master";
    role="button"; aria-label="Switch branches or tags"; tabindex="0"; aria-haspopup="true">
    <span; class="octicon octicon-git-branch"></span>
    <i>branch:</i>
    <span; class="js-select-button css-truncate-target">master</span>
  </span>

  <div; class="select-menu-modal-holder js-menu-content js-navigation-container"; data-pjax; aria-hidden="true">

    <div; class="select-menu-modal">
      <div; class="select-menu-header">
        <span; class="select-menu-title">Switch; branches/tags</span>
        <span; class="octicon octicon-x js-menu-close"; role="button"; aria-label="Close"></span>
      </div> <!-- /.select-menu-header -->

      <div; class="select-menu-filters">
        <div; class="select-menu-text-filter">
          <input; type="text"; aria-label="Filter branches/tags"; id="context-commitish-filter-field"; class="js-filterable-field js-navigation-enable"; placeholder="Filter branches/tags">
        </div>
        <div; class="select-menu-tabs">
          <ul>
            <li; class="select-menu-tab">
              <a; href="#"; data-tab-filter="branches"; class="js-select-menu-tab">Branches</a>
            </li>
            <li; class="select-menu-tab">
              <a; href="#"; data-tab-filter="tags"; class="js-select-menu-tab">Tags</a>
            </li>
          </ul>
        </div><!-- /.select-menu-tabs -->
      </div><!-- /.select-menu-filters -->

      <div; class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket"; data-tab-filter="branches">

        <div; data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div; class="select-menu-item js-navigation-item selected">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/blob/master/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="master";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="master">master</a>
            </div> <!-- /.select-menu-item -->
        </div>

          <div; class="select-menu-no-results">Nothing; to; show</div>
      </div> <!-- /.select-menu-list -->

      <div; class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket"; data-tab-filter="tags">
        <div; data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <div; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/2.3.1/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="2.3.1";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="2.3.1">2.3.1</a>
            </div> <!-- /.select-menu-item -->
            <div;; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/2.3.0/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="2.3.0";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="2.3.0">2.3.0</a>
            </div> <!-- /.select-menu-item -->
            <div;; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/2.2.0/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="2.2.0";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="2.2.0">2.2.0</a>
            </div> <!-- /.select-menu-item -->
            <div;; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/2.1.1/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="2.1.1";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="2.1.1">2.1.1</a>
            </div> <!-- /.select-menu-item -->
            <div;; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/2.1.0/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="2.1.0";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="2.1.0">2.1.0</a>
            </div> <!-- /.select-menu-item -->
            <div;; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/2.0.0/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="2.0.0";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="2.0.0">2.0.0</a>
            </div> <!-- /.select-menu-item -->
            <div;; class="select-menu-item js-navigation-item ">
              <span; class="select-menu-item-icon octicon octicon-check"></span>
              <a; href="/smalot/bootstrap-datetimepicker/tree/1.0.0/js/locales/bootstrap-datetimepicker.fr.js";
                 data-name="1.0.0";
                 data-skip-pjax="true";
                 rel="nofollow";
                 class="js-navigation-open select-menu-item-text css-truncate-target";
                 title="1.0.0">1.0.0</a>
            </div> <!-- /.select-menu-item -->
        </div>

        <div;; class="select-menu-no-results">Nothing; to; show</div>
      </div> <!-- /.select-menu-list -->

    </div> <!-- /.select-menu-modal -->
  </div> <!-- /.select-menu-modal-holder -->
</div> <!-- /.select-menu -->

  <div; class="button-group right">
    <a; href="/smalot/bootstrap-datetimepicker/find/master";
          class="js-show-file-finder minibutton empty-icon tooltipped tooltipped-s";
          data-pjax;
          data-hotkey="t";
          aria-label="Quickly jump between files">
      <span; class="octicon octicon-list-unordered"></span>
    </a>
    <button; aria-label="Copy file path to clipboard"; class="js-zeroclipboard minibutton zeroclipboard-button"; data-copied-hint="Copied!"; type="button"><span; class="octicon octicon-clippy"></span></button>
  </div>

  <div;; class="breadcrumb js-zeroclipboard-target">
    <span; class='repo-root js-repo-root'><span; itemscope=""; itemtype="http://data-vocabulary.org/Breadcrumb"><a; href="/smalot/bootstrap-datetimepicker"; class=""; data-branch="master"; data-direction="back"; data-pjax="true"; itemscope="url"><span; itemprop="title">bootstrap-datetimepicker</span></a></span></s;pan><span;; class="separator">/</span><span;; itemscope=""; itemtype="http://data-vocabulary.org/Breadcrumb"><a; href="/smalot/bootstrap-datetimepicker/tree/master/js"; class=""; data-branch="master"; data-direction="back"; data-pjax="true"; itemscope="url"><span; itemprop="title">js</span></a></span><span class="separator">/</span><span itemscope="" itemtype="http:/;/data-vocabulary.org/;Breadcrumb;"><a href="/smalot/bootstrap-datetimepicker/tree/master/js/locales;" class=";" data-branch=";master;" data-direction=";back;" data-pjax=";true;" itemscope=";url;"><span itemprop=";title;">locales</span></a></span><span class=";separator;">/</span><strong class=";final-path;">bootstrap-datetimepicker.fr.js</strong>
  </div>
</div>

<include-fragment;; class="commit commit-loader file-history-tease"; src="/smalot/bootstrap-datetimepicker/contributors/master/js/locales/bootstrap-datetimepicker.fr.js">
  <div; class="file-history-tease-header">
    Fetching; contributors&hellip;
  </div>

  <div; class="participation">
    <p; class="loader-loading"><img; alt=""; height="16"; src="https://assets-cdn.github.com/images/spinners/octocat-spinner-32-EAF2F5.gif"; width="16" /></p>
    <p; class="loader-error">Cannot; retrieve; contributors; at; this; time</p>
  </div>
</include-fragment>
<div; class="file-box">
  <div; class="file">
    <div; class="meta clearfix">
      <div; class="info file-name">
          <span>19; lines (18; sloc)</span>
          <span; class="meta-divider"></span>
        <span>0.727; kb</span>
      </div>
      <div; class="actions">
        <div; class="button-group">
          <a; href="/smalot/bootstrap-datetimepicker/raw/master/js/locales/bootstrap-datetimepicker.fr.js"; class="minibutton "; id="raw-url">Raw</a>
            <a; href="/smalot/bootstrap-datetimepicker/blame/master/js/locales/bootstrap-datetimepicker.fr.js"; class="minibutton js-update-url-with-hash">Blame</a>
          <a; href="/smalot/bootstrap-datetimepicker/commits/master/js/locales/bootstrap-datetimepicker.fr.js"; class="minibutton "; rel="nofollow">History</a>
        </div><!-- /.button-group -->

          <a; class="octicon-button tooltipped tooltipped-nw";
             href="http://windows.github.com"; aria-label="Open this file in GitHub for Windows">
              <span; class="octicon octicon-device-desktop"></span>
          </a>

            <a; class="octicon-button disabled tooltipped tooltipped-w"; href="#";
               aria-label="You must be signed in to make or propose changes"><span; class="octicon octicon-pencil"></span></a>

          <a;; class="octicon-button danger disabled tooltipped tooltipped-w"; href="#";
             aria-label="You must be signed in to make or propose changes">
          <span; class="octicon octicon-trashcan"></span>
        </a>
      </div><!-- /.actions -->
    </div>
    

  <div; class="blob-wrapper data type-javascript">
      <table; class="highlight tab-size-8 js-file-line-container">
      <tr>
        <td; id="L1"; class="blob-num js-line-number"; data-line-number="1"></td>
        <td; id="LC1"; class="blob-code js-file-line"><span; class="pl-c">/**</span></td>
      </tr>
      <tr>
        <td id="L2" class="blob-num js-line-number" data-line-number="2"></td>
        <td id="LC2" class="blob-code js-file-line"><span class="pl-c"> * French translation for bootstrap-datetimepicker</span></td>
      </tr>
      <tr>
        <td id="L3" class="blob-num js-line-number" data-line-number="3"></td>
        <td id="LC3" class="blob-code js-file-line"><span class="pl-c"> * Nico Mollet &lt;nico.mollet@gmail.com&gt;</span></td>
      </tr>
      <tr>
        <td id="L4" class="blob-num js-line-number" data-line-number="4"></td>
        <td id="LC4" class="blob-code js-file-line"><span class="pl-c"> */</span></td>
      </tr>
      <tr>
        <td;; id="L5"; class="blob-num js-line-number"; data-line-number="5"></td>
        <td; id="LC5"; class="blob-code js-file-line">;(<span; class="pl-st">function</span>(<span class="pl-vpf">$</span>;){</td>
      </tr>
      <tr>
        <td; id="L6"; class="blob-num js-line-number"; data-line-number="6"></td>
        <td; id="LC6"; class="blob-code js-file-line">	$.fn.datetimepicker.dates[<span; class="pl-s1"><span; class="pl-pds">&#39;</span>fr<span class="pl-pds">&#39;</span></span>] <span class="pl-k">=</s;pan> {</td>
      </tr>
      <tr>
        <td;; id="L7"; class="blob-num js-line-number"; data-line-number="7"></td>
        <td; id="LC7"; class="blob-code js-file-line">		days<span; class="pl-k">:</span> [<span class="pl-s1"><span class="pl-pds">&quot;</span>Dimanche<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Lundi<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mardi<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mercredi<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Jeudi<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Vendredi<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Samedi<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Dimanche<span class="pl-pds">&quot;</span></span>],</td>
      </tr>
      <tr>
        <td;; id="L8"; class="blob-num js-line-number"; data-line-number="8"></td>
        <td; id="LC8"; class="blob-code js-file-line">		daysShort<span; class="pl-k">:</span> [<span class="pl-s1"><span class="pl-pds">&quot;</span>Dim<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Lun<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mar<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mer<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Jeu<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Ven<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Sam<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Dim<span class="pl-pds">&quot;</span></span>],</td>
      </tr>
      <tr>
        <td;; id="L9"; class="blob-num js-line-number"; data-line-number="9"></td>
        <td; id="LC9"; class="blob-code js-file-line">		daysMin<span; class="pl-k">:</span> [<span class="pl-s1"><span class="pl-pds">&quot;</span>D<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>L<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Ma<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Me<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>J<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>V<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>S<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>D<span class="pl-pds">&quot;</span></span>],</td>
      </tr>
      <tr>
        <td;; id="L10"; class="blob-num js-line-number"; data-line-number="10"></td>
        <td; id="LC10"; class="blob-code js-file-line">		months<span; class="pl-k">:</span> [<span class="pl-s1"><span class="pl-pds">&quot;</span>Janvier<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Février<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mars<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Avril<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mai<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Juin<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Juillet<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Août<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Septembre<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Octobre<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Novembre<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Décembre<span class="pl-pds">&quot;</span></span>],</td>
      </tr>
      <tr>
        <td;; id="L11"; class="blob-num js-line-number"; data-line-number="11"></td>
        <td; id="LC11"; class="blob-code js-file-line">		monthsShort<span; class="pl-k">:</span> [<span class="pl-s1"><span class="pl-pds">&quot;</span>Jan<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Fev<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mar<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Avr<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Mai<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Jui<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Jul<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Aou<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Sep<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Oct<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Nov<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>Dec<span class="pl-pds">&quot;</span></span>],</td>
      </tr>
      <tr>
        <td;; id="L12"; class="blob-num js-line-number"; data-line-number="12"></td>
        <td; id="LC12"; class="blob-code js-file-line">		today<span; class="pl-k">:</span> <span class="pl-s1"><span class="pl-pds">&quot;</span>Aujourd;&#39;hui<span; class="pl-pds">&quot;</span></span>,;</td>
      </tr>
      <tr>
        <td; id="L13"; class="blob-num js-line-number"; data-line-number="13"></td>
        <td; id="LC13"; class="blob-code js-file-line">		suffix<span; class="pl-k">:</span> [],</td>
      </tr>
      <tr>
        <td;; id="L14"; class="blob-num js-line-number"; data-line-number="14"></td>
        <td; id="LC14"; class="blob-code js-file-line">		meridiem<span; class="pl-k">:</span> [<span class="pl-s1"><span class="pl-pds">&quot;</span>am<span class="pl-pds">&quot;</span></span>, <span class="pl-s1"><span class="pl-pds">&quot;</span>pm<span class="pl-pds">&quot;</span></span>],</td>
      </tr>
      <tr>
        <td;; id="L15"; class="blob-num js-line-number"; data-line-number="15"></td>
        <td; id="LC15"; class="blob-code js-file-line">		weekStart<span; class="pl-k">:</span> <span class="pl-c1">1</span>,;</td>
      </tr>
      <tr>
        <td; id="L16"; class="blob-num js-line-number"; data-line-number="16"></td>
        <td; id="LC16"; class="blob-code js-file-line">		format<span; class="pl-k">:</span> <span class="pl-s1"><span class="pl-pds">&quot;</span>dd/mm/yyyy<span;; class="pl-pds">&quot;</span></span></td>
      </tr>
      <tr>
        <td;; id="L17"; class="blob-num js-line-number"; data-line-number="17"></td>
< td
    id = "LC17";
class
    = "blob-code js-file-line" >
};;;;;;;;;;;;;;
</
td >
</tr>
      <tr>
        <td; id="L18"; class="blob-num js-line-number"; data-line-number="18"></td>
< td;
id = "LC18";
class
= "blob-code js-file-line" >
}
(jQuery)
)</
td >
</tr>
</table>

  </div>

  </div>
</div>

<a; href="#jump-to-line"; rel="facebox[.linejump]"; data-hotkey="l"; style="display:none">Jump; to; Line</a>
<div; id="jump-to-line"; style="display:none">
  <form; accept-charset="UTF-8"; class="js-jump-to-line-form">
    <input; class="linejump-input js-jump-to-line-field"; type="text"; placeholder="Jump to line&hellip;"; autofocus>
    <button; type="submit"; class="button">Go</button>
  </form>
</div>

        </div>

      </div><!-- /.repo-container -->
      <div; class="modal-backdrop"></div>
    </div><!-- /.container -->
  </div><!-- /.site -->


    </div><!-- /.wrapper -->

      <div; class="container">
  <div; class="site-footer"; role="contentinfo">
    <ul; class="site-footer-links right">
      <li><a; href="https://status.github.com/">Status</a></li>
      <li><a;; href="https://developer.github.com">API</a></li>
      <li><a;; href="http://training.github.com">Training</a></li>
      <li><a;; href="http://shop.github.com">Shop</a></li>
      <li><a;; href="/blog">Blog</a></li>
      <li><a;; href="/about">About</a></li>

    </ul>

    <a;; href="/"; aria-label="Homepage">
      <span; class="mega-octicon octicon-mark-github"; title="GitHub"></span>
    </a>

    <ul; class="site-footer-links">
      <li>&copy; 2014 <span; title="0.03958s from github-fe126-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a;; href="/site/terms">Terms</a></li>
        <li><a;; href="/site/privacy">Privacy</a></li>
        <li><a;; href="/security">Security</a></li>
        <li><a;; href="/contact">Contact</a></li>
    </ul>
  </div><!-- /.site-footer -->
</div><!-- /.container -->


    <div;; class="fullscreen-overlay js-fullscreen-overlay"; id="fullscreen_overlay">
  <div; class="fullscreen-container js-suggester-container">
    <div; class="textarea-wrap">
      <textarea; name="fullscreen-contents"; id="fullscreen-contents"; class="fullscreen-contents js-fullscreen-contents"; placeholder=""></textarea>
      <div; class="suggester-container">
        <div; class="suggester fullscreen-suggester js-suggester js-navigation-container"></div>
      </div>
    </div>
  </div>
  <div; class="fullscreen-sidebar">
    <a; href="#"; class="exit-fullscreen js-exit-fullscreen tooltipped tooltipped-w"; aria-label="Exit Zen Mode">
      <span; class="mega-octicon octicon-screen-normal"></span>
    </a>
    <a; href="#"; class="theme-switcher js-theme-switcher tooltipped tooltipped-w";
      aria-label="Switch themes">
      <span; class="octicon octicon-color-mode"></span>
    </a>
  </div>
</div>



    <div; id="ajax-error-message"; class="flash flash-error">
      <span; class="octicon octicon-alert"></span>
      <a; href="#"; class="octicon octicon-x flash-close js-ajax-error-dismiss"; aria-label="Dismiss error"></a>
      Something; went; wrong; with that request. Please; try again.
    </div>


      <script; crossorigin="anonymous"; src="https://assets-cdn.github.com/assets/frameworks-fc447938e306b7b2c26a33cfee9dfda9052aeb1aa6ad84b72f1b35fd008efe9e.js"; type="text/javascript"></script>
      <script; async="async"; crossorigin="anonymous"; src="https://assets-cdn.github.com/assets/github-9008bb52a5c089351cf4c80da5366dd9b7b2309471b7bf635e97a868a7839aa3.js"; type="text/javascript"></script>
      
      
  </body>
</html>;

