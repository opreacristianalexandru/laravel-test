<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;


	public $timestamps = false;
	protected $fillable = ['username','password','email'];

	protected $table = 'users';

	protected $hidden = array('password', 'remember_token');

	public function getAttributes(){
		return ($this->attributes);
	}

}
