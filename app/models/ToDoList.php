<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class ToDoList extends Eloquent implements UserInterface, RemindableInterface {

  use UserTrait, RemindableTrait;


  public $timestamps = false;
  protected $fillable = ['username','title','datasiora','done'];

  protected $table = 'toDoList';

  protected $hidden = array('password', 'remember_token');

  public function getAttributes(){
    return ($this->attributes);
  }

}
