<?php

/*
**	Ruta pentru teste , urmeaza a fi stearsa
*/
		Route::get('/createfakeuser',function()
		{
			$user = new User;
			$user->id = '1';
			$user->username = 'cretzu';
			$user->password = Hash::make('123');
			$user->email = '123';
			$user->save();
		});

/*
 *   Log
 */

$monolog = Log::getMonolog();
$syslog = new \Monolog\Handler\SyslogHandler('papertrail');
$formatter = new \Monolog\Formatter\LineFormatter('%channel%.%level_name%: %message% %extra%');
$syslog->setFormatter($formatter);

$monolog->pushHandler($syslog);


/*
** Sesiune
*/

		Route::get('login',[
			'as'   => 'login',
			'uses' => 'SessionsController@create'
		]);

		Route::get('logout',[
			'as'   => 'logout',
			'uses' => 'SessionsController@destroy'
		]);
		Route::group(array('before' => 'auth'), function()
		{
			Route::get('admin',[
				'as'   => 'dashboard',
				'uses' => 'SessionsController@admin'
			]);
		});

/*
**	Users
*/
		Route::group(array('before' => 'auth'), function()
		{
			Route::get('users/create',[
					'as'   => 'user_create',
					'uses' => 'UsersController@create'
			]);
			Route::get('users/delete/{id}',[
					'as'   => 'user_delete',
					'uses' => 'UsersController@delete'
			]);
			Route::get('users',[
					'as'   => 'users_index',
					'uses' => 'UsersController@index'
			]);
		});


/*
**	To Do List
*/

		Route::group(array('before' => 'auth'), function()
		{
			Route::get  ('todolist',[
					'as'   => 'todolist_index',
					'uses' => 'ToDoListController@todolist'
			]);
			Route::get  ('todolist/createtodolist',[
					'as'   => 'todolist_get_create',
					'uses' => 'ToDoListController@createTDL'
			]);
			Route::post ('todolist/createtodolist',[
					'as'   => 'todolist_post_create',
					'uses' => 'ToDoListController@tdl'
			]);
			Route::get  ('todolist/undone/{id}',[
					'as'   => 'todolist_make_undone',
					'uses' => 'ToDoListController@unfinishedTD'
			]);
			Route::get  ('todolist/done/{id}',[
					'as'   => 'todolist_make_done',
					'uses' => 'ToDoListController@finishedTD'
			]);
			Route::get  ('todolist/delete/{id}',[
					'as'   => 'todolist_delete',
					'uses' => 'ToDoListController@delete'
			]);
			Route::get  ('todolist/calendar',[
					'as'   => 'todolist_calendar',
					'uses' => 'ToDoListController@calendar'
			]);
			Route::get  ('todolist/modify/{id}',[
					'as'   => 'todolist_get_modifiy',
					'uses' => 'ToDoListController@modifyTDL'
			]);
			Route::post ('todolist/modify/{id}',[
					'as'   => 'todolist_post_modify',
					'uses' => 'ToDoListController@modifyTD'
			]);
			Route::get  ('todolist/subscribe/{id}',[
					'as'	 => 'todolist_subscribe',
					'uses' => 'ToDoListController@subscribe'
				]);
			Route::get  ('todolist/unsubscribe/{id}',[
					'as'   => 'todolist_unsubscribe',
					'uses' => 'ToDoListController@unsubscribe'
				]);
		});


/*
**	Change password
*/

		Route::group(array('before' => 'auth'), function()
		{
			Route::get  ('verify',[
					'as'   => 'verify_get_password',
					'uses' => 'AccountController@createVerify'
			]);
			Route::post ('verify',[
					'as'   => 'verify_post_password',
					'uses' => 'AccountController@verify'
			]);
		});


/*
**	Resources
*/

		Route::resource('sessions','SessionsController');
		Route::resource('users','UsersController');
		Route::resource('account','AccountController');
		Route::resource('','HomeController');
