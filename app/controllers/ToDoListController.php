<?php


class ToDoListController extends BaseController
{
	public function __construct(Apps $app,UserService $user)
	{
		$this->user = $user;
		$this->app = $app;
		parent::__construct();
	}
	public function toDoList()
	{
		return View::make('sessions.tdl',['tdl' => $this->user->getFullTDL(),'dt'=>$this->user->dt->format('Y-m-d'),'currentUser' =>  Auth::user()->username]);
	}

	public function createTDL()
	{
		return View::make('sessions.createtdl');
	}

	public function tdl()
	{
		$this->app->addToDo(Input::all());
		return Redirect::to('/todolist');
	}

	public function finishedTD($id)
	{
		$this->app->removeToDo($id);
		return Redirect::to('/todolist');
	}

	public function unfinishedTD($id)
	{
		$this->app->modifyToDo($id);
		return Redirect::to('/todolist');
	}

	public function delete($id)
	{
		return Redirect::to('/todolist')->with($this->app->deleteToDo($id));
	}

	public function calendar()
	{
		return View::make('sessions.calendar',array('tdl' => $this->user->getTDL()),array('dt'=>$this->user->dt->format('Y-m-d')));
	}

	public function modifyTDL($id)
	{
		return View::make('sessions.modifytdl',['td' => $this->user->getTD($id),'id' => $id]);
	}

	public function modifyTD($id)
	{
		$this->user->sendMail($id,Input::all());
		return Redirect::to('/todolist');
	}

	public function subscribe($id)
	{
		$this->app->subscribe($id);
		return Redirect::to('/todolist');
	}

	public function unsubscribe($id)
	{
		$this->app->unsubscribe($id);
		return Redirect::to('/todolist');
	}
}
