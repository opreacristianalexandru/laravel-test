<?php


class SessionsController extends BaseController
{

  // Controler ce se ocupa strict de activitatile utilizatorilor deja conectati
  // sau ce urmeaza a se conecta.

  protected $user1;
  protected $app;
  // Este nevoie de obiectul de tip Login pentru a putea face verificarea si
  // autentificarea corecta a utilizatorului.
  // Este nevoie si de un obiect de tip Apps pentru a permite unui utilizator
  // ce este deja logat sa isi verifice/modifice/creeze un ToDoList
  public function __construct(Login $user1,Apps $app)
  {
    $this->user1 = $user1;
    $this->app = $app;
    // $this->controllerName='sessions';
    parent::__construct();
  }

  // Functia index verifica strict daca utilizatorul este conectat , iar daca
  // nu este conectat acesta este redirectionat la pagina de LogIn.
  public function index()
  {
  if(Auth::check()) return View::make('sessions.admin');
  return View::make('sessions.create');
  }

  // Functia create creeaza view-ul pentru LogIn in cazul in care utilizatorul
  // nu este conectat
  public function create()
  {
  if(Auth::check()) return Redirect::to('/admin');
  return View::make('sessions.create');
  }

  // Functia store face verificarea/autentificarea utilizatorului pe baza
  // datelor completate in view-ul sessions.create .
  public function store()
  {
  $input = Input::all();
    if($this->user1->fill($input)->isValid())
    {
      return Redirect::back()->withInput()->withErrors($this->user1->errors);
    }
    if(!Auth::attempt(Input::only('username','password')))
    {
      return Redirect::back()->withInput();
    }
  return Redirect::to('/admin');
  }

  // Functia destroy face delogarea utilizatorului curent
  public function destroy()
  {
    Auth::logout();
    return Redirect::to('/login');
  }

  // Functia admin creeaza o pagina personalizata pentru fiecare utilizator
  // logat din care acesta sa aiba acces la toate functiile pregatite pentru
  // utilizatorii conectati.
  public function admin()
  {
    return View::make('sessions.admin',['username'=> Auth::user()->username]);
  }
}
