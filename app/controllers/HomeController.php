<?php

class HomeController extends BaseController
{
	// Contoler pentru pagina de index (http://localhost:8000/) ce
	// verifica face o verificare daca persoana ce incearca sa se
	// conecteze la site este deja autentificata , iar daca nu este
	// il trimite in mod automat la view-ul pentru LogIn

	public function __construct()
	{
		parent::__construct();	
	}

	public function index()
	{
		if(Auth::check()) return Redirect::to('/admin');
		return View::make('sessions.create');
	}
}
