<?php

class UsersController extends HomeController
{
	// Controler ce se ocupa de partea de managementul utilizatorilor

	private $userService;
	public function __construct(UserService $userService,Apps $app)
	{
		$this->app = $app;
		$this->userService = $userService;
		parent::__construct();
	}

	// Functia index afiseaza intr-un view atat toti utilizatorii (cu posibilitatea de a sterge pe cei ce
	// nu sunt logati) dar ofera si posibilitatea de a crea un utilizator
	public function index()
	{
		if(Auth::check())
		{
			$users = $this->app->getUsers();
			$currentUserName = Auth::user()->username;
			return View::make('users/index', array('users' => $users, 'currentUserName' => $currentUserName));
		}
		return View::make('sessions.create');
	}

	// Functia create genereaza view-ul pentru crearea utilizatorilor
	public function create()
	{
		if(Auth::check())
		{
			return View::make('users.create');
		}
		return View::make('sessions.create');
	}

	// Functia store creaza un utilizator in functie de datele introduse in view-ul users.create
	public function store()
	{
		$validationErrors = $this->userService->createUser(Input::all());
		if($validationErrors)
		{
			return Redirect::back()->withInput()->withErrors($validationErrors);
		}
		return Redirect::to('/users');
	}

	// Functia delete sterge utilizatorul selectat din lista
	public function delete($id)
	{
	return Redirect::to('/users')->with($this->userService->deleteUser($id));
  }

}
