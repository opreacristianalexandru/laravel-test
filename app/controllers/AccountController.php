<?php

// Controlerul AccountController se ocupa doar de partea de modificarea parolei utilizatorului curent logat
class AccountController extends SecureController
{
  private $userService;
  public function __construct(UserService $userService)
  {
    parent::__construct();
    $this->userService = $userService;
  }

  // Functia index creeaza view-ul pentru modificarea parolei utilizatorului curent logat
  public function index()
  {
    return View::make('account.index');
  }

  // Functia store este functia ce se ocupa de modificarea parolei utilizatorului curent
  public function store()
  {
    $this->userService->modifyUser();
    return Redirect::to('/admin');
  }

  public function createVerify()
  {
    return View::make('account.verify');
  }

  public function verify()
  {
    if(Hash::check(array_get(Input::all(),'password'),Auth::user()->password))
    {
      return Redirect::to('/account');
    }
    else if($this->userService->checkChangePassword(Auth::user()->username,Input::all()))
    {
      return Redirect::back()->withInput()->withErrors($this->userService->errors);
    }
    return Redirect::back();
  }
}
