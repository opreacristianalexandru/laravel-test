<?php

class BaseController extends Controller {

public $view;

	public function __construct()
{
	$this->view = new Apps();
	$this->view->currentView();
}

	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}
