<?php namespace Laravel\Composers;

class DefaultViewComposer {

	public function compose($view)
	{
		$view->with('currentController', ['action'=>Route::currentRouteAction()]);
	}

}
