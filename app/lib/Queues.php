<?php
use Carbon\Carbon;

class Queues{

	public function __construct(Apps $app)
	{
		$this->app = $app;
	}
	public function mailQueue($idtbd,$oldDate,$newDate,$event)
	{
		$subbedUsers = $this->app->TDsubscriptions($idtbd);
		if(count($subbedUsers)!=0)
		{
		foreach($subbedUsers as $user)
			{
			$date = Carbon::now()->addSeconds(10);
			Queue::later($date,'SendMailService@fire',['id' => $user->userid , 'oldDate' => $oldDate , 'newDate' => $newDate , 'event' => $event]);
			}
		}
	}
}
