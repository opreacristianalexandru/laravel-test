<?php

class UserService
{
  public $errors;
  public $utilizator;
  public $subbedTD;

  public static $rulesCheckPass=
  [
  'username' => 'required',
  'password' => 'required'
  ];

  public static $rules=
  [
    'username' => 'required',
    'password' => 'required',
    'email' => 'required'
  ];

  public function __construct(User $user,Apps $app,Queues $queue)
  {
    $this->dt = new DateTime();
    $this->utilizator = $user;
    $this->queue = $queue;
    $this->app = $app;
  }

  public function isValidEmail(User $user)
  {
    $validator = Validator::make
    (
      array(
        'email' => $user->email
      ),
      array(
        'email' => 'email'
      )
    );
    if ($validator->fails())
    {
      $this->errors = $validator->messages();
      return true;
    }
    return false;
  }

  public function isValidCheckPass(User $user)
  {
    $validation = Validator::make($user->getAttributes(),static::$rulesCheckPass);
    if($validation->passes())
    {
      return false;
    }
    $this->errors = $validation->messages();
    return true;
  }

  public function isValid(User $user)
  {
    $validation = Validator::make($user->getAttributes(),static::$rules);
      if($validation->passes())
      {
        return false;
      }
    $this->errors = $validation->messages();
    return true;
  }

  public function createUser($inputAll)
  {
    $user = new User;
      if($this->isValid($user->fill($inputAll)))
      {
        return $this->errors;
      }
      if($this->isValidEmail($user->fill($inputAll)))
      {
      return $this->errors;
      }
    $user->username = array_get($inputAll,'username');
    $user->password = Hash::make(array_get($inputAll,'password'));
    $user->email = array_get($inputAll,'email');
    try
    {
      $user->save();
    }
    catch(Exception $ex)
    {
    Validator::extend('duplicat', function($attribute, $value, $parameters)
    {
      if($value==!'null')
      return true;
      return false;
    });
    $validator = Validator::make
    (
      array('username/email' => $ex),
      array('username/email' => array('duplicat'))
    );
    $this->errors = $validator->messages();
    return $this->errors;
    }
    return null;
  }

  public function modifyUser()
  {
    $parola = Input::get('password');
    DB::table('users')
      ->where('username', Auth::user()->username)
      ->update(array('password' => Hash::make($parola)));
  }

  public function delete($id)
  {
    $affectedRows = DB::table('users')->where('id', '=', $id)->delete();
    return $affectedRows;
  }

  public function deleteUser($id)
  {
    $utilizator = DB::table('users')->where('id','=', $id)->pluck('username');
    $affectedRows = $this->delete($id);
    $message = array(
      'error' => !$affectedRows ? 'User '.$utilizator.' not found' : null,
      'success' => !$affectedRows ? null : 'User '.$utilizator.' was deleted', );
    return $message;
  }

  public function checkChangePassword($username,$input)
  {
    $this->utilizator->username = $username;
    $this->utilizator->password = array_get($input,'password');
    if($this->isValidCheckPass($this->utilizator))
    {
      return $this->errors;
    }
    return false;
  }

  public function getUser()
  {
    return $this->utilizator;
  }

  public function getTDL()
  {
    $currentUser = Auth::user()->username;
    $tdl = DB::table('toDoList')
        ->orderBy('datasiora','asc')
        ->where('username', '=', $currentUser)
        ->orwhere('public','=','true')
        ->get();
    return $tdl;

  }
  public function paginateTDL()
  {
    $currentUser = Auth::user()->username;
    $tdl = DB::table('toDoList')
      ->orderBy('datasiora','asc')
      ->where('username', '=', $currentUser)
      ->orwhere('public','=','true')
        ->paginate(3);
    return $tdl;
  }

  public function isSubbed()
  {
    $isSubbed = $this->app->subscriptions();
    $counter = 0;
    foreach($this->paginateTDL() as $td)
    {
      foreach($isSubbed as $sub)
      {
        if($td->id == $sub->tdid)
        {
          $this->subbedTD[$counter] = 1;
          break;
        }
        else
        {
          $this->subbedTD[$counter] = 0;
        }
      }
      $counter = $counter + 1;
    }
    return $this->subbedTD;
  }

  public function getFullTDL()
  {
    $TDL = $this->paginateTDL();
//    dd($TDL);
    $sub = $this->isSubbed();
    for($i = 0 ; $i<count($TDL); $i++){
      $TDL[$i] = (object) array_merge((array) $TDL[$i], ['subbed' => $sub[$i]]);
  }
  return $TDL;
  }

  public function getTD($id)
  {
    $currentUser = Auth::user()->username;
    $td = DB::table('toDoList')
    ->orderBy('datasiora','asc')
    ->where('username', '=', $currentUser)
    ->where('id','=', $id)
    ->first();
    return $td;
  }

  public function sendMail($id,$input)
  {
    $oldDate = $this->getTD($id)->datasiora;
    $this->app->changeDateToDo($id,$input);
    $newDate = $this->getTD($id)->datasiora;
    $event = $this->getTD($id)->title;
    $this->queue->mailQueue($id,$oldDate,$newDate,$event);
  }
}
