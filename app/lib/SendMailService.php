<?php

class SendMailService
{

	public function __construct(Mails $mail,Apps $app)
	{
		$this->app = $app;
		$this->mail = $mail;
	}

	public function fire($job, $data)
	{
		$this->subject = 'New date on event';
		$this->mail->sendMail($this->app->getUser($data['id'])->email,$data['oldDate'],$data['newDate'],$data['event'],$this->subject);
		$job->delete();
	}

}
