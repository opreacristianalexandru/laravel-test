<?php

  class Apps{

  protected static $rules=
  [
    'title' => 'required',
    'datasiora' => 'required',
  ];
  public $loginRules=
  [
    'username' => 'required',
    'password' => 'required'
  ];

  public function loginIsValid()
  {
    $validation = Validator::make($this->attributes,static::$loginRules);
    if($validation->passes())
    {
      return false;
    }
    $this->errors = $validation->messages();
    return true;
  }

  public function isValidTD(ToDoList $tdl)
  {
    $validation = Validator::make($tdl->getAttributes(),static::$rules);
    if($validation->passes())
    {
      return false;
    }
    $this->errors = $validation->messages();
    return true;
  }

  public function makeBool($inputAll)
  {
    if(array_get($inputAll,'public')==null)
    return false;
    else
    return true;
  }

  public function addToDo($inputAll)
  {

    $newTDL = new ToDoList;
    if($this->isValidTD($newTDL->fill($inputAll)))
  {
    return $this->errors;
  }
    $newTDL->username = Auth::user()->username;
    $newTDL->title = array_get($inputAll,'title');
    $newTDL->datasiora = array_get($inputAll,'datasiora');
    $newTDL->done = 'false';
    $newTDL->public = $this->makeBool($inputAll);
    $newTDL->save();
    return null;
  }

  public function delete($id)
  {
    $affectedRows = DB::table('toDoList')->where('id', '=', $id)->delete();
    return $affectedRows;
  }

  public function deleteTDSubscriptions($id)
  {
    DB::table('subscriptions')->where('tdid','=',$id)->delete();
    return ;
  }

  public function deleteToDo($id)
  {
    $toDoItem = DB::table('toDoList')->where('id','=', $id)->pluck('title');
    $affectedRows = $this->delete($id);
    $this->deleteTDSubscriptions($id);
    // dd(!$affectedRows);
    $message = array(
      'error' => !$affectedRows ? $toDoItem.' not found' : null,
      'success' => !$affectedRows ? null : $toDoItem.' was deleted', );
      return $message;
    }

  public function removeToDo($id)
  {
    DB::table('toDoList')
    ->where('id', $id)
    ->update(array('done' => 'true'));
  }

  public function modifyToDo($id)
  {
    DB::table('toDoList')
    ->where('id', $id)
    ->update(array('done' => 'false'));
  }

  public function currentView()
  {
    View::composer('*', function($view)
    {
      // dd(Route::currentRouteAction());
    switch(Route::currentRouteAction())
      {
        case 'HomeController@index':
        case 'SessionsController@create':
          $this->controllerName = 'login';
        break;
        case 'SessionsController@admin':
          $this->controllerName = 'admin';
        break;
        case 'SessionsController@todolist':
        case 'SessionsController@createTDL':
          $this->controllerName = 'todolist';
        break;
        case 'AccountController@show':
        case 'AccountController@verify':
        case 'AccountController@createVerify':
        case 'UsersController@index':
        case 'UsersController@create':
          $this->controllerName = 'users';
        break;
        default:
        // dd('ceva');
          $this->controllerName = 'users';
      }
          // dd($this->controllerName);
    if(Auth::check())
    {
      $this->user = 'Signed in as '.Auth::user()->username;
    }
    else
    {
      $this->user = '';
    }
      return $view->with('currentController', [
        'name'=>$this->controllerName,
        'user' => $this->user
      ]);
      });
  }

  public function getUsers(){
    $users = DB::table('users')->paginate(2);
    return $users;
  }

  public function getUser($id)
  {
    $user = DB::table('users')->where('id', '=', $id)->first();
    return $user;
  }

  public function getUserByTBD($id)
  {
    $userfromtbd = DB::table('toDoList')
    ->where ('id','=',$id)
    ->first();
    $user = DB::table('users')
    ->where('username', '=' , $userfromtbd->username )
    ->first();
    return $user;
  }

  public function changeDateToDo($id,$inputAll)
  {
    DB::table('toDoList')
    ->where('id', '=' , $id)
    ->update(array('datasiora' => array_get($inputAll,'datasiora')));
  }

  public function subscribe($idTD)
  {
    $newSub = new Subscription;
    $newSub->userid = Auth::user()->id;
    $newSub->tdid = $idTD;
    $newSub->save();
    return null;
  }

  public function subscriptions()
  {
    $subscriptions = DB::table('subscriptions')
    ->leftJoin('toDoList', function($join)
    {
      $join->on('subscriptions.tdid', '=', 'toDoList.id')
      ->where('subscriptions.userid', '=', Auth::user()->id);
    })
    ->get();
    return $subscriptions;
  }

  public function TDsubscriptions($id)
  {
    $subscriptions = DB::table('subscriptions')
    ->leftJoin('toDoList','subscriptions.tdid', '=', 'toDoList.id')
    ->where('subscriptions.tdid', '=', $id)
    ->get();
    return $subscriptions;
  }

  public function unsubscribe($id)
  {
    $affectedRows = DB::table('subscriptions')->where('tdid', '=', $id)->delete();
    return $affectedRows;
  }

}
