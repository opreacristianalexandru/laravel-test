<!DOCTYPE html>
<html>
    <head>
        <title>
            @section('title')
Prefix pentru titlu -
            @show
        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- CSS are placed here -->
        {{ HTML::style('css/bootstrap.css') }}
        {{ HTML::style('css/bootstrap-theme.css') }}
        {{ HTML::style('css/bootstrap-datetimepicker.min.css')}}
        {{ HTML::style('css/bootstrap.min.css')}}
        <style>
        body {
          #calendar {
            max-width: 900px;
            margin: 0 auto;
          }
        @section('styles')
        @show
        </style>
        {{ HTML::script('js/jquery-1.11.1.min.js') }}
        {{ HTML::script('js/bootstrap.min.js') }}
        {{ HTML::script('js/bootstrap-datetimepicker.min.js') }}
        <link rel='stylesheet' href='../css/jquery-ui.min.css' />
        <link href='../css/fullcalendar.css' rel='stylesheet' />
        <link href='../css/fullcalendar.print.css' rel='stylesheet' media='print' />
        <script src='../js/moment.min.js'></script>
        <script src='../js/fullcalendar.min.js'></script>
        @include('sessions.datepickerjs')
    </head>

    <body>

    @section('nav')
    @include('layouts.navbar',$currentController)
    @show

        <!-- Generic handler for error / success flashes -->
        @if (Session::has('error'))
        Error: {{Session::get('error')}}
        @endif
        @if (Session::has('success'))
        Success: {{Session::get('success')}}
        @endif
        <!-- Container -->
        <div class="container">

            <!-- Content -->
            @yield('content')

        </div>

        <!-- Scripts are placed here -->

    </body>
</html>
