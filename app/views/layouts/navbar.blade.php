<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-left">
				<li role="presentation" @if($currentController['name']=='admin'){{"class=\"active\""}}@endif><a href="/admin">Admin</a></li>
				<li role="presentation" @if($currentController['name']=='todolist'){{"class=\"active\""}}@endif><a href="/todolist">To Do List</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" >Users <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="/users/create">Create User</a></li>
						<li class="divider"></li>
						<li><a href="/users">Check Users</a></li>
					</ul>
				</li>
			</ul>
				<ul class="nav navbar-nav navbar-right">
					<li class="navbar-text">{{$currentController['user']}}</li>
				</ul>
			</div><!-- /.navbar-collapse -->
		</div>
	</nav>
