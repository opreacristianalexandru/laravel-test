@extends('layouts.master')
@section('title')
To Do List
@stop
@section('content')

<table class="table table-condesed">
    <thead>
      <tr>
        <th>
          Title
        </th>
        <th>
          Date
        </th>
        <th>
          Author
        </th>
        <th>
          Done?
        </th>
        <th>
          Delete
        </th>
        <th>
          Subscribe
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tdl as $td)
      @if ($dt>$td->datasiora)
      @include('sessions.todop1',array($td,$currentUser))
      @else
      @include('sessions.todop2',array($td,$currentUser))
      @endif
      @endforeach
    </tbody>
    {{$tdl->links()}}

</table>

<button class="btn" type="button" onclick="window.location='{{url("todolist/calendar")}}'">Calendar</button>
<button class="btn" type="button" onclick="window.location='{{url("todolist/createtodolist")}}'">Create To Do</button>
<button class="btn" type="button" onclick="window.location='{{url("admin")}}'">Back</button>

@stop
