@extends('layouts.master')
@section('title')
  Admin Page
  @stop
@section('content')
    {{Form::open()}}
      {{Form::label('Welcome ')}} {{$username}}
    <div id="create user">
      <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{url("users/")}}'">Users/Create User</button>
    </div>
    <div id="reset password">
    <button type="button" class="btn btn-sm btn-primary" onclick="window.location='{{url("/verify")}}'">Reset Password</button>
    </div>
    <div id="tdl">
    <button type="button" class="btn btn-sm btn-info" onclick="window.location='{{url("/todolist")}}'">To Do List</button>
    </div>
    <div id="Logout">
      <button type="button" class="btn btn-sm btn-danger" onclick="window.location='{{url("/logout")}}'">Logout</button>
    </div>
    {{Form::close()}}
@stop
