@extends('layouts.master')
@section('title')
Create To Do List
@stop
@section('content')
{{Form::open(['url'=> "todolist/modify/$id",'class'=>'form-horizontal'])}}
<div id="tdlTitle">
	<div class="form-group">
		<label for="title" class="control-label col-sm-2">Modify "To Do"</label>
		<div class="col-sm-10">
			<input type="text" class="form-control" id="title" value="{{$td->title}}" name="title" readonly>
		</div>
	</div>
</div>

<div id="tdlPublic">
	<div class="form-group">
		<label for="public" class="control-label col-sm-2">Public </label>
		<div class="col-sm-10">
			@if($td->public == true)
			{{Form::checkbox('public',true)}}
			@else
			{{Form::checkbox('public')}}
			@endif
		</div>
	</div>
</div>

<div class="form-group">
	<label for="dtp_input2" class="col-md-2 control-label">Date Picking</label>
	<div class="input-group date form_date col-md-5" data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="mm-dd-yyyy">
		<input class="form-control" size="16" type="text" value="{{$td->datasiora}}" readonly name="datasiora">
		<span class="input-group-addon"><span class="glyphicon glyphicon-remove"></span></span>
		<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
	</div>
	<input type="hidden" id="dtp_input2" value="" /><br/>
</div>
<div id="tdlSubmit">
	{{Form::submit('Modify this ToDo',['class' => 'btn btn-success'])}}
</div>
{{Form::close()}}
<button class="btn" type="button" onclick="window.location='{{url("todolist")}}'">Back</button>
@stop
