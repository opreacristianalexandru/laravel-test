@extends('layouts.master')
@section('title')
Login
@stop
@section('content')
{{Form::open(['route'=>'sessions.store','class'=>'form-horizontal'])}}

<div class="form-group">
  <label for="username" class="control-label col-sm-2">Username</label>
  <div class="col-sm-10">
  <input type="text" class="form-control" id="username" placeholder="Enter Username" name ="username">
  </div>
</div>
{{$errors->first('username')}}

<div class="form-group">
  <label for="password" class="control-label col-sm-2">Password</label>
  <div class="col-sm-10">
  <input type="password" class="form-control" id="password" placeholder="Password" name="password">
  </div>
</div>
{{$errors->first('password')}}

<div class="form-group">
  <div class="col-sm-offset-2 col-sm-10">
    <button type="submit" class="btn btn-default">Sign in</button>
  </div>
</div>
{{Form::close()}}
@stop
