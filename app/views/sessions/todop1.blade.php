<tr class="danger">
	<td>
		{{$td->title}}
	</td>
	<td>
		{{$td->datasiora}}
	</td>
	<td>
		@if($td->username!=$currentUser)
			{{$td->username}}
		@else
			Me
		@endif
	</td>
	<td>
		@if($td->done=='false')
		<button type="button" class="btn btn-xs btn-danger" onclick="window.location='{{url("todolist/done/".$td->id)}}'">Is UnDone</button>
		@else
		<button type="button" class="btn btn-xs btn-success" onclick="window.location='{{url("todolist/undone/".$td->id)}}'">Is Done</button>
		@endif
	</td>
	<td>
		<a class="btn btn-danger btn-xs" href="{{url("todolist/delete/".$td->id)}}">Delete item</a>
	</td>
	<td>
		@if($td->username!=$currentUser)
			@if($td->subbed == '1')
			<button type="button" class="btn btn-xs btn-danger" onclick="window.location='{{url("todolist/unsubscribe/".$td->id)}}'">Unsubscribe</button>
			@else
			<button type="button" class="btn btn-xs btn-success" onclick="window.location='{{url("todolist/subscribe/".$td->id)}}'">Subscribe</button>
			@endif
		@else
		<a class="btn btn-info btn-xs" href="{{url("todolist/modify/".$td->id)}}">Modify TDL</a>
		@endif
	</td>
</tr>
