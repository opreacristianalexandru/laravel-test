@extends('layouts.master')
@section('title')
Calendar ToDo
@stop
@section('content')
<script>

$(document).ready(function() {

	$('#calendar').fullCalendar({
		theme: true,
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
		defaultDate: '{{$dt}}',
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
		@foreach ($tdl as $td)
		{
			title: '{{$td->title}}',
			start: '{{$td->datasiora}}',
			color: 
					@if($td->done=='false')
						'red'
					@else
					'#257e4a'
					@endif
		},
		@endforeach
		{
			title: 'blank',
			start: '1-1-2000'
		}
		]
	});

});

</script>
<div id='calendar'></div>

<button class="btn" type="button" onclick="window.location='{{url("todolist/createtodolist")}}'">Create To Do</button>
<button class="btn" type="button" onclick="window.location='{{url("todolist")}}'">Back</button>
@stop
