@extends('layouts.master')
@section('title')
Create User
@stop
@section('content')
<body>
  {{Form::open(['route'=>'users.store','class'=>'form-horizontal'])}}
  {{$errors->first('username/email')}}
      <div class="form-group">
        <label for="username" class="control-label col-sm-2">Username</label>
        <div class="col-sm-10">
        <input type="username" class="form-control" id="username" placeholder="Enter Username" name="username">
        </div>
      </div>
    {{$errors->first('username')}}

      <div class="form-group">
        <label for="email" class="control-label col-sm-2">Email address</label>
        <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
        </div>
      </div>
    {{$errors->first('email')}}

      <div class="form-group">
        <label for="password" class="control-label col-sm-2">Password</label>
        <div class="col-sm-10">
        <input type="password" class="form-control" id="password" placeholder="Password" name="password">
        </div>
      </div>
    {{$errors->first('password')}}

  <div id="create">
    {{Form::submit('Create',['class' => 'btn btn-success'])}}
  </div>

  <div id="name">
    <button type="button" class="btn" onclick="window.location='{{url("/users")}}'">Back</button>
  </div>
  {{Form::close()}}
</body>
@stop
