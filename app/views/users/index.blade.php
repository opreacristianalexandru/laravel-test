@extends('layouts.master')
@section('title')
All users
@stop
@section('content')
  <h1>All Users</h1>
  @if ($users->isEmpty())
  whatever
  @else

  <table class="table table-condesed">
    <thead>
      <tr>
        <th>
          Username
        </th>
        <th>
          Email
        </th>
        <th>
          Delete?
        </th>
      </tr>
    </thead>
    <tbody>
      @foreach ($users as $user)
      <tr>
        <td>
          {{$user->username}}
        </td>
        <td>
          {{$user->email}}
        </td>
        <td>
          @if ($user->username != $currentUserName)
          <a class="btn btn-danger btn-xs" href="{{url("users/delete/".$user->id)}}">Delete {{$user->username}}</a>
          @else
          <span class="badge">this is you!</span>
          @endif
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  @endif
  {{$users->links()}}
  <button class="btn" type="button" onclick="window.location='{{url("users/create")}}'">Create User</button>
  <button class="btn" type="button" onclick="window.location='{{url("admin")}}'">Back</button>
@stop
