@extends('layouts.master')
@section('title')
Change your password
@stop
@section('content')
<h1>Reset your password</h1>
{{Form::open(['route'=>'account.store','class'=>'form-horizontal'])}}
<label for="password" class="control-label col-sm-2">Enter your new password</label>
<div class="col-sm-10">
  <input type="password" class="form-control" id="password" placeholder="Your new password" name="password">
</div>
  {{Form::submit('Click to reset',['class'=>'btn btn-danger'])}}
  <button type="button" class="btn btn-info" onclick="window.location='{{url("/admin")}}'">Back</button>
{{Form::close()}}
@stop
