@extends('layouts.master')
@section('title')
  Check your password
  @stop
@section('content')
{{Form::open(['url'=>URL::to('verify'),'class'=>'form-horizontal'])}}
<h1>Check your password</h1>
<label for="password" class="control-label col-sm-2">Enter your current password</label>
<div class="col-sm-10">
  <input type="password" class="form-control" id="password" placeholder="Your current password" name="password">
</div>
  {{Form::submit('Continue',['class'=>'btn btn-warning'])}}
  <button type="button" class="btn btn-info" onclick="window.location='{{url("/admin")}}'">Back</button>
{{Form::close()}}
@stop
