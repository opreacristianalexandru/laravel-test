<?php

class UserTableSeeder extends Seeder {

	public function run()
	{
		DB::table('users')->delete();

		User::create(array(
			'id' => '1',
			'username' => 'admin',
			'password' => Hash::make('admin'),
			'email' => 'admin@localhost.com'
			));
	}

}
