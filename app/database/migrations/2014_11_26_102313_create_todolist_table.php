<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodolistTable extends Migration {

	public function up()
	{
		Schema::create('toDoList', function(Blueprint $table)
			{
				$table->increments('id');
				$table->string('username');
				$table->string('title');
				$table->date('datasiora');
				$table->string('done');
			});
	}


	public function down()
	{
		Schema::table('toDoList', function(Blueprint $table)
		{
			//
		});
	}

}
